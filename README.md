# Random Assignment Generator

Have a list of names that you want to randomize and pair up for some assignments?
This program is the solution. Supply the list of names, the number of folks in
each group, and the number of assignments. This program will generate a
randomized list of groupings for each assignment. The program attempts to minimize
repeated groupings. Run it like so:

    (random_people_assigner) Enterprise:assign-people-randomly$ ./make_assignments.py --help
    usage: make_assignments.py [-h] --number-of-assignments NUMBER_OF_ASSIGNMENTS
                           --number-of-partners {1,2,3,4,5,6,7,8,9}

    Find names in the 'names' file and randomly assign those names with the
    specified number of partners.

    optional arguments:
    -h, --help            show this help message and exit

    required arguments:
    --number-of-assignments NUMBER_OF_ASSIGNMENTS
                        The number assignments to which names will be randomly
                        distributed
    --number-of-partners {1,2,3,4,5,6,7,8,9}
                        The number of partners per assignment
