#! /usr/bin/env python3

import argparse
import random
import sys

class Person:
    '''A person that can be assigned to things and have partners'''
    def __init__(self, name):
        self.name = name
        self.previous_partners = []
        self.number_assignments = 0

    def addPartner(self, partner):
        if partner not in self.previous_partners:
            self.previous_partners.append(partner)
            partner.addPartner(self)
            return True
        return False

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class NumAssignmentsRangeChecker(argparse.Action):
    '''Used to range-check the specified number of assignments'''
    def __call__(self, parser, namespace, values, option_string=None):
        if values < 1 or values > sys.maxsize:
            parser.error(
                'Valid range of values for {} is [1,MAXINT]'.format(option_string)
            )
        setattr(namespace, self.dest, values)


def parseArgs():
    parser = argparse.ArgumentParser(
        description='Find names in the \'names\' file ' \
                    'and randomly assign those names with ' \
                    'the specified number of partners.'
    )
    required = parser.add_argument_group('required arguments')
    required.add_argument(
        '--number-of-assignments',
        type=int,
        action=NumAssignmentsRangeChecker,
        required=True,
        help='The number assignments to which names will be randomly distributed',
    )
    required.add_argument(
        '--number-of-partners',
        type=int,
        choices=range(1, 10),
        required=True,
        help='The number of partners per assignment',
    )
    args = parser.parse_args()
    return args

def getRandomPerson(list_of_people):
    try:
        return list_of_people.pop(random.randint( 0, len(list_of_people)-1 ))
    except ValueError:
        return list_of_people.pop()

def makeAssignments(list_of_people, numberOfAssignments):
    '''
    This function really should handle assignments to groups of n partners in size.
    Right now, though, it just handles groups of two partners, effectively ignoring
    the --number-of-partners command line option.
    '''
    partners = []

    random.seed()
    while len(partners) < numberOfAssignments:
        try:
            person1 = getRandomPerson(list_of_people)
        except IndexError:
            return partners

        while True:
            already_tried = []
            try:
                person2 = getRandomPerson(list_of_people)
            except IndexError:
                return partners

            try:
                if person1.addPartner(person2):
                    partners.append((person1,person2))
                    list_of_people += already_tried
                    break;
                elif len(list_of_people) > 1:
                    # these people have already been partners, put person2 back on
                    # the list and try for another pairing
                    already_tried.append(person2)
                else:
                    # there aren't enough people left in the list for another pairing
                    return partners
            except AttributeError:
                pass

def main():
    args = parseArgs()

    people = []

    with open('./names', 'r') as input_file:
        for name in input_file:
            people.append(Person(name.rstrip()))

    # call makeAssignments() with a copy of the list until we have enough
    # sets of partners
    partners = []
    while len(partners) < args.number_of_assignments:
        partners += makeAssignments(list(people), args.number_of_assignments)

    [print('{} & {}'.format(x[0],x[1])) for x in partners]
    # [print(x) for x in partners]

    sys.exit(0)

if __name__ == '__main__':
    main()
